#include "Area.h"

/**
* A constructor by default.
*/
Area::Area()
{

}

/**
* A copy constructor.
*/
Area::Area(const Area& area)
{
	Point1 = area.Point1;
	Point2 = area.Point2;
}

/**
* A constructor by parameters.
* @p1 is a Place. This is the left top point of the destination.
* @p1 is a Place. This is the left top point of the destination.
*/
Area::Area(Place p1, Place p2)
{
	Point1 = p1;
	Point2 = p2;
}

/**
* A destructor.
*/
Area::~Area()
{

}


/**
* A method set a Place object to Point1.
*/
void Area::SetPoint1(Place p)
{
	Point1 = p;
}

/**
* A method set a Place object to Point2.
*/
void Area::SetPoint2(Place p)
{
	Point2 = p;
}

/**
* A method get Point1.
*/
Place Area::GetPoint1()
{
	return Point1;
}

/**
* A method get Point2.
*/
Place Area::GetPoint2()
{
	return Point2;
}

/**
*
*/
Area & Area::operator+=(const Area & paramAera)
{
	Point1 = paramAera.Point1;
	Point2 = paramAera.Point2;
	return *this;
}

/**
* override of << operator.
* prints the information of the Area object.
*/
std::ostream& operator << (std::ostream&os, Area&a)
{
	Place p1(a.GetPoint1());
	Place p2(a.GetPoint2());
	
	os << "Area zone = " << " " << p1 << " ; " << p2 << " ";
	return os;
}