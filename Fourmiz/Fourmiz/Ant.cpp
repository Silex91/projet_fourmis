#include "Ant.h"
#include "Place.h"

unsigned int Ant::nextIdAnt = 1;


/**
* A default constructor.
*/
Ant::Ant() : walkAngle(0), direction(true), speed(1)
{
	idAnt = nextIdAnt;
	nextIdAnt++;
	//------------------------------------
	location = new Place();
	//------------------------------------
	safeDistance = DEFAULT_SAFEDISTANCE;
	energy = DEFAULT_ENERGY;
	enthusiasm = DEFAULT_ENTHUSIASM;
	//------------------------------------
	destination = Area();
}

/**
* A parameters constructor.
* @p : A Place argument.
* @angle : A Integer argument.
* @dir : A boolean argument.
* @speed : An integer argument.
* @distance : An integer argument.
*/
Ant::Ant(Place *p, Area dest, int angle, bool dir, double spd, int distance)
{
	idAnt = nextIdAnt;
	nextIdAnt++;
	//------------------------------------
	location = p;
	walkAngle = angle;
	direction = dir;
	speed = spd;
	safeDistance = distance;
	//------------------------------------
	energy = DEFAULT_ENERGY;
	enthusiasm = DEFAULT_ENTHUSIASM;
	//------------------------------------
	destination = dest;
}

/**
* A copy constructor.
*/
Ant::Ant(const Ant& a)
{
	idAnt = nextIdAnt;
	nextIdAnt++;
	//------------------------------------
	location = new Place(*a.location);
	walkAngle = a.walkAngle;
	direction = a.direction;
	speed = a.speed;
	safeDistance = a.safeDistance;
	destination = a.destination;
	//------------------------------------
	energy = DEFAULT_ENERGY;
	enthusiasm = DEFAULT_ENTHUSIASM;
}
/**
* A destructor.
* Frees @location.
*/
Ant::~Ant()
{
	if (location != NULL)
		delete location;
}


/*********************************************************************************************/

/**
* Getter @idAnt.
*/
unsigned int Ant::GetId() { return idAnt; }


/**
* Getter @location.
*/
Place Ant::GetLocation() { return *location; }

/**
* Setter location.
*/
void Ant::SetLocation(Place p)
{
	if (location)
		delete location;
	location = new Place(p);
}

Area Ant::Getdestination() {
	return destination;
}

void Ant::Setdestination(Area destinationParam) {
	destination = destinationParam;
}

/**
* Getter location x position.
*/
int Ant::GetXLocation() { return location->GetX(); }

/**
* Setter location x position.
*/
void Ant::SetXLocation(int x) { location->SetX(x); }

/**
* Getter location y position.
*/
int Ant::GetYLocation() { return location->GetY(); }

/**
* Setter location y position.
*/
void Ant::SetYLocation(int y) { location->SetY(y); }

/**
* Getter walkAngle.
*/
int Ant::GetWalkAngle() { return walkAngle; }

/**
* Setter walkAngle.
*/
void Ant::SetWalkAngle(int angle) { walkAngle = angle; }

/**
* Getter direction.
*/
bool Ant::GetDirection() { return direction; }

/**
* Setter direction.
*/
void Ant::SetDirection(bool b) { direction = b; }

/**
* Getter speed.
*/
double Ant::GetSpeed() { return speed; }

/**
* Setter speed.
*/
void Ant::SetSpeed(double s) { speed = s; }

/**
* Getter energy.
*/
double Ant::GetEnergy() { return energy; }

/**
* Setter energy.
*/
void Ant::SetEnergy(double paramEnergy) { energy = paramEnergy; }

/**
* Getter enthusiasm.
*/
double Ant::GetEnthusiasm(){ return enthusiasm; }

/**
* Setter enthusiasm.
*/
void Ant::SetEnthusiasm(double paramEnthusiasm) { enthusiasm = paramEnthusiasm;  }

/**
* Getter safeDistance.
*/
int Ant::GetSafeDistance() { return safeDistance; }

/**
* Setter safeDistance.
*/
void Ant::SetSafeDistance(int distance) { safeDistance = distance; }





