#pragma once

#include "Place.h"
#include "Obstacle.h"
#include <FL/Fl.H>
#include <FL/fl_draw.H>

#define DEFAULT_XPOSITION 10;
#define DEFAULT_YPOSITION 10;
#define DEFAULT_WSIZE 100;
#define DEFAULT_HSIZE 100;

/**
* A class used for draw Obstacle.
* This class inherits Fl_Widget in the FLTK library.
* It allows to draw obstacles using FLTK.
*/
class DrawingObstacle : public Fl_Widget
{
private:
	static int xPosition;
	static int yPosition;
	static int wSize;
	static int hSize;

	/**
	* A pointer on an Obstacle attribute.
	* It contains all informations about location of the obstacle.
	*/
	Obstacle * obstacle;

public:
	/**
	* A constructor by default.
	*/
	DrawingObstacle();

	/**
	* A constructor by parameters.
	*/
	DrawingObstacle(int, int, int, int);

	/**
	* A constructor by parameters.
	*/
	DrawingObstacle(int, int, int, int, Obstacle*);

	/**
	* A constructor by parameters.
	*/
	DrawingObstacle(Obstacle*);

	/**
	* A destructor.
	* Does nothing.
	*/
	~DrawingObstacle();

	/**
	* @overise
	* A method with no arguments and returning nothing.
	* This is the method called when we need to draw the widget.
	*/
	void draw();

	/**
	* Getter obstacle.
	*/
	Obstacle* GetObstacle();

	/**
	* Setter obstacle.
	*/
	void SetObstacle(Obstacle*);
};

