#pragma once
#include "Place.h"

/**
* A class allows to manipulate obstacles.
* It allows to create obstacles.
*/
class Obstacle
{
private:
	/**
	* A static Integer attribute.
	* it is incremented each time an obstacle is created.
	*/
	static int nextIdObstacle;

	/**
	* An Integer attribute.
	* The id of the obstacle.
	*/
	int idObstacle;

	/**
	* An Integer attribute.
	* numberPoint value is the number of points of the outiline of the obstacle.
	*/
	int numberPoint;

	/**
	* A Place pointer attribute.
	* listPoint is a pointer on a table of Place containing the positions of the outline of the obstacle.
	*/
	Place ** listPoint;

	/**
	* A boolean attribute.
	* Its value is "TRUE" if it can be climbed.
	* "FALSE" otherwise.
	*/
	bool canBeClimbed;

public:
	/**
	* A constructor by default.
	*/
	Obstacle();

	/**
	* A copy constructor.
	*/
	Obstacle(const Obstacle&);

	/**
	* A constructor by parameters.
	*/
	Obstacle(bool);

	/**
	* A constructor by parameters.
	*/
	Obstacle(int, Place**);

	/**
	* A constructor by parameters.
	*/
	Obstacle(int, Place**, bool);

	/**
	* A destruuctor.
	* frees @listpoint.
	*/
	~Obstacle();

	/*********************************************************************************************/
	/**
	* A method adding a Place object in the listPoint.
	*/
	void AddPoint(Place*);

	/**
	* A method removing a Place in the listPoint.
	*/
	void RemovePoint(int);

	/**
	* A method clearing the listPoint.
	*/
	void ClearListPoint();

	/*********************************************************************************************/
	/**
	* Getter canBeClimbed.
	*/
	bool GetCanBeClimbed() const;

	/**
	* Setter canBeClimbed.
	*/
	void SetCanBeClimbed(bool);

	/**
	* Getter numberPoint.
	*/
	int GetNumberPoint() const;

	/**
	* Setter numberPoint.
	*/
	void SetNumberPoint(int);

	/**
	* Getter listPoint.
	*/
	Place** GetListPoint() const;

	/**
	* Setter listPoint.
	*/
	void SetListPoint(Place**);

	/**
	* Getter listPoint[i].
	*/
	Place * GetPoint(int) const;

	/**
	* Setter listPoint[i].
	*/
	void SetPoint(int, Place*);

	/**
	* Getter id.
	*/
	int GetId();

	/**
	* Setter id.
	*/
	void SetId(int);
};

