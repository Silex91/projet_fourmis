#include "Map.h"
#include<iostream>

/**
* A default constructor.
*/
Map::Map() 
	:numberAnt(0), numberGroup(0), numberObstacle(0)
{
	listObstacle = NULL;
	listGroup = NULL;
}

/**
* A copy constructor.
*/
Map::Map(const Map &m)
{
	numberObstacle = m.numberObstacle;
	listObstacle = new Obstacle*[numberObstacle];
	for (int i = 0; i < numberObstacle; i++)
	{
		listObstacle[i] = new Obstacle(m.listObstacle[i]);
	}
	numberGroup = m.numberGroup;
	listGroup = new GroupAnt*[numberGroup];
	for (int i = 0; i < numberGroup; i++)
	{
		listGroup[i] = new GroupAnt(*m.listGroup[i]);
	}
	numberAnt = m.numberAnt;
}

/**
* A parameters constructor.
*/
Map::Map(int nbObstacle, Obstacle ** listObs, int nbGroup, GroupAnt ** listGr)
{
	numberGroup = nbGroup;
	numberObstacle = nbObstacle;

	listGroup = listGr;
	listObstacle = listObs;

	numberAnt = 0;
	for (int i = 0; i < numberGroup; i++)
		numberAnt += listGroup[i]->GetnumberAnt();
}

/**
* The destructor of Map class.
*/
Map::~Map()
{
	for (int i = 0; i < numberObstacle; i++)
	{
		if (listObstacle[i] != NULL)
			delete listObstacle[i];
	}
	if (listObstacle)
		delete[] listObstacle;

	for (int i = 0; i < numberGroup; i++)
	{
		if (listGroup[i] != NULL)
			delete listGroup[i];
	}
	if (listGroup)
		delete[] listGroup;
}


/**
* Adds an Ant in the first group of @listGroup.
* @a a pointer on the Ant to add.
*/
void Map::AddAnt(Ant* a)
{
	if (numberGroup == 0 || !listGroup)
	{
		//throws exception
		return;
	}
	listGroup[0]->addAnt(a);
}

/**
* Remove an Ant in the group of @listGroup which contains the ant.
*/
void Map::RemoveAnt(unsigned int idAnt)
{
	for (int i = 0; i < numberGroup; i++)
	{
		listGroup[i]->removeAnt(idAnt);
	}

}

/**
* Removes an Ant in the specified group of @listGroup.
* @idAnt an unsigned int parameter : the id of the ant.
* @idGroup an unsigned int parameter : the id of the GroupAnt.
*/
void Map::RemoveAnt(unsigned int idAnt, unsigned int idGroup)
{
	for (int i = 0; i < numberGroup; i++)
	{
		if(listGroup[i]->GetidGroup() == idGroup) 
			listGroup[i]->removeAnt(idAnt);
	}
}


/**
* Adds an Ant in the specified group of @listGroup.
* @a a pointer on the ant to add.
* @idGr the id of the group to add @a.
*/
void Map::AddAnt(Ant* a, unsigned int idGr)
{
	if (numberGroup == 0 || !listGroup)
	{
		//throws exception
		return;
	}
	int index = -1;
	for (int i = 0; i < numberGroup; i++)
		if (listGroup[i]->GetidGroup() == idGr)
			index = i;
	if (index == -1)
	{
		//throws exception
		return;
	}
	listGroup[index]->addAnt(a);
	numberAnt++;
}

/**
* Add a GroupAnt in the list.
* @o : A pointer on a GroupAnt object
*/
void Map::AddGroup(GroupAnt *g)
{
	if (!g)
	{
		//throws exception
		return;
	}
	GroupAnt** temp = new GroupAnt*[numberGroup + 1];
	for (int i = 0; i < numberGroup; i++)
	{
		temp[i] = listGroup[i];
	}
	temp[numberGroup] = g;
	if (listGroup)
		delete[] listGroup;
	listGroup = temp;
	numberGroup++;

	numberAnt += g->GetnumberAnt();
}

/**
* Removes a group in the list.
*@id is the id of the group to remove in @listGroup.
*/
void Map::RemoveGroup(int id)
{
	int index = -1;
	for (int i = 0; i < numberGroup; i++)
	{
		if (listGroup[i]->GetidGroup() == id)
			index = i;
	}
	if (index == -1)
	{
		//throw exception
		return;
	}
	GroupAnt** temp = new GroupAnt *[numberAnt - 1];
	for (int i = 0; i < index; i++)
	{
		temp[i] = listGroup[i];
	}
	for (int i = index + 1; i < numberGroup; i++)
	{
		temp[i - 1] = listGroup[i];
	}
	numberAnt -= listGroup[index]->GetnumberAnt();
	if (listGroup[index])
		delete listGroup[index];
	if (listGroup)
		delete[] listGroup;
	listGroup = temp;
	numberGroup++;
}

/**
* Clears all the groups of @listGroup.
*
*/
void Map::ClearGroups()
{
	for (int i = 0; i < numberGroup; i++)
	{
		if (listGroup[i])
			delete listGroup[i];
	}
	if (listGroup)
		delete[] listGroup;
	listGroup = NULL;
	numberGroup = 0;
	numberAnt = 0;
}

/**
* Add an obstacle in the list.
* @o : A pointer on an Obstacle attribute
*/
void Map::AddObstacle(Obstacle *o)
{
	Obstacle ** newList = new Obstacle*[numberObstacle + 1];
	for (int i = 0; i < numberObstacle; i++)
		newList[i] = listObstacle[i];
	newList[numberObstacle] = o;

	if (numberObstacle > 0)
		delete listObstacle;
	listObstacle = newList;
	numberObstacle++;
}

/**
* Removes an obstacle in the list @listObstacle.
*@id the id of the obstacle to remove.
*/
void Map::RemoveObstacle(int id)
{
	int index = -1;
	for (int i = 0; i < numberObstacle; i++)
		if (listObstacle[i]->GetId() == id)
			index = i;
	if (index == -1)
	{
		return;
	}
	
	Obstacle ** newList = new Obstacle*[numberObstacle - 1];
	for (int i = 0; i < index; i++)
		newList[i] = listObstacle[i];
	for (int i = index + 1; i < numberObstacle; i++)
		newList[i - 1] = listObstacle[i];
	delete listObstacle;
	listObstacle = newList;
}

/**
* Clears all the obstacles in @listObstacle.
*
*/
void Map::ClearObstacles()
{
	for (int i = 0; i < numberObstacle; i++)
	{
		if (listObstacle[i])
			delete listObstacle[i];
	}
	delete[] listObstacle;
	listObstacle = NULL;
	numberObstacle = 0;
}



//---------------Getter and Setter --------------------------------------------

/**
* Getter of numberObstacle
*/
int Map::GetNumberObstacle()
{
	return numberObstacle;
}

/**
* Setter of numberOstacle
*/
void Map::SetNumberobstacle(int n)
{
	numberObstacle = n;
}

/**
* Getter of listObstacle
*/
Obstacle** Map::GetListObstacle()
{
	return listObstacle;
}

/**
* Setter of listObstacle
*/
void Map::SetListObstacle(Obstacle** lo)
{
	if (listObstacle)
	{
		for (int i = 0; i < numberObstacle; i++)
			if (listObstacle[i])
				delete listObstacle[i];
		delete[] listObstacle;
	}

	listObstacle = lo;
}

/**
* Getter of numberGroup
*/
int Map::GetNumberGroup()
{
	return numberGroup;
}

/**
* Setter of numberGroup
*/
void Map::SetNumberGroup(int n)
{
	numberGroup = n;
}

/**
* Getter of listGroup
*/
GroupAnt** Map::GetListGroup()
{
	return listGroup;
}

/**
* Setter of listGroup
*/
void Map::SetListGroup(GroupAnt** lg)
{
	if (listGroup)
	{
		for (int i = 0; i < numberGroup; i++)
			if (listGroup[i])
				delete listGroup[i];
		delete[] listGroup;
	}
	listGroup = lg;
}

/**
* Getter of numberAnt
*/
int Map::GetnumberAnt()
{
	return numberAnt;
}

/**
* Setter of numberAnt
*/
void Map::SetNumberAnt(int n)
{
	numberAnt = n;
}

