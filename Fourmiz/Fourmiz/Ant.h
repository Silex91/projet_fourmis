#pragma once
#include<iostream>
#include "Place.h"
#include "Area.h"

/**
* The default value of @safeDistance.
*/
#define DEFAULT_SAFEDISTANCE 10;

/**
* The default value of @speed.
*/
#define DEFAULT_SPEED 0;

/**
* The default value of @walkAngle.
*/
#define DEFAULT_ANGLE 0;

/**
* The default value of @walkAngle.
*/
#define DEFAULT_ENERGY 1;

/**
* The default value of @walkAngle.
*/
#define DEFAULT_ENTHUSIASM 0;

/**
* A class allows to manipulate obstacles.
* It allows to create obstacles.
*/
class Ant
{
private:
	/**
	* A static Integer attribute.
	* it is incremented each time an ant is created.
	*/
	static unsigned int nextIdAnt;

	/**
	* An Integer attribute.
	* The id of the ant.
	*/
	int unsigned idAnt;

	/**
	* A pointer attribute on a Place.
	* It contains the location of the ant on the map.
	*/
	Place * location;

	Area destination;

	/**
	* An Integer attribute.
	* Its value is the angle of the direction of the ant.
	*/
	int walkAngle;

	/**
	* A boolean attribute.
	* It takes the value TRUE if the ant is going to its destination.
	* It takes FASLE if the ant come back home.
	*/
	bool direction;

	/**
	* An integer attribute.
	* The value of the ant's speed.
	*/
	double speed;

	/**
	* An dooble attribute.
	* The value of the ant's energy between 0 and 1 that will impact it's speed.
	*/
	double energy;

	/**
	* An dooble attribute.
	* The value of the ant's energy between 0 and 1 that will impact it's walkAngle.
	*/
	double enthusiasm;

	/**
	* An integer attribute.
	* Its value is the distance that the ant try to keep between an other ant.
	*/
	int safeDistance;

public:

	/**
	* A default constructor.
	*/
	Ant();

	/**
	* A parameters constructor.
	*/
	Ant(Place* location, Area destination, int angle, bool goToDest, double speed, int safe); // Location , Zone , Angle , Direction , Vitesse , SafeDistance

	/**
	* A copy consructor.
	*/
	Ant(const Ant&);

	/**
	* A destructor.
	* Frees @location.
	*/
	~Ant();

	/*********************************************************************************************/
	
	/**
	* Getter @idAnt.
	*/
	int unsigned GetId();

	/**
	* Getter @location.
	*/
	Place GetLocation();

	/**
	* Setter location.
	*/
	void SetLocation(Place);

	Area Getdestination();

	void Setdestination(Area destinationParam);

	/**
	* Getter location x position.
	*/
	int GetXLocation();

	/**
	* Setter location x position.
	*/
	void SetXLocation(int);

	/**
	* Getter location y position.
	*/
	int GetYLocation();

	/**
	* Setter location y position.
	*/
	void SetYLocation(int);

	/**
	* Getter walkAngle.
	*/
	int GetWalkAngle();

	/**
	* Setter walkAngle.
	*/
	void SetWalkAngle(int);

	/**
	* Getter direction.
	*/
	bool GetDirection();

	/**
	* Setter direction.
	*/
	void SetDirection(bool);

	/**
	* Getter speed.
	*/
	double GetSpeed();

	/**
	* Setter speed.
	*/
	void SetSpeed(double);

	/**
	* Getter energy.
	*/
	double GetEnergy();

	/**
	* Setter energy.
	*/
	void SetEnergy(double);

	/**
	* Getter enthusiasm.
	*/
	double GetEnthusiasm();

	/**
	* Setter enthusiasm.
	*/
	void SetEnthusiasm(double);

	/**
	* Getter safeDistance.
	*/
	int GetSafeDistance();

	/**
	* Setter safeDistance.
	*/
	void SetSafeDistance(int);

	

};

