#include "MotorAction.h"
#include <iostream>
#include <time.h>

/**
* A default constructor
*/
MotorAction::MotorAction()
	: numberDrawingAnt(0), numberDrawingObstacle(0), map(NULL)
{
	windowsMap = new Fl_Double_Window(1600, 900);
	Fl::visual(FL_DOUBLE | FL_INDEX);
	listDrawingAnt = NULL;
	listDrawingObstacle = NULL;
	windowsMap->end();
	windowsMap->show();
	RedrawObstacles();
	RedrawAnts();
}

/**
* A constructor
*/
MotorAction::MotorAction(Map * m)
{
	windowsMap = new Fl_Double_Window(900, 400);
	Fl::visual(FL_DOUBLE | FL_INDEX);
	map = new Map(*m);
	Obstacle** listObstacle = map->GetListObstacle();
	GroupAnt** listGroup = map->GetListGroup();

	listDrawingObstacle = new DrawingObstacle*[map->GetNumberObstacle()];
	for (int i = 0; i < map->GetNumberObstacle(); i++)
	{
		listDrawingObstacle[i] = new DrawingObstacle(listObstacle[i]);
	}

	listDrawingAnt = new DrawingAnt*[map->GetnumberAnt()];
	int k = 0;
	for (int i = 0; i < map->GetNumberGroup(); i++)
	{
		Ant** listAnt = listGroup[i]->GetlistAnt();
		for (unsigned int j = 0; j < listGroup[i]->GetnumberAnt(); j++, k++)
		{
			listDrawingAnt[k] = new DrawingAnt(listAnt[j]);
		}
	}
	windowsMap->end();
	windowsMap->show();
	RedrawObstacles();
	RedrawAnts();

}

/**
* The destructor of MotorAction class
*/
MotorAction::~MotorAction()
{
	if (numberDrawingAnt != 0)
		delete listDrawingAnt;
	if (numberDrawingObstacle != 0)
		delete listDrawingObstacle;
	if (map != NULL)
		delete map;
	if (windowsMap != NULL)
		delete windowsMap;
}

//===================== Gestion des fourmis et des obstacles ================================//

/**
* This method redraws all the ants of the map on the windows
*/
void MotorAction::RedrawAnts()
{
	for (int i = 0; i < numberDrawingAnt; i++)
	{
		listDrawingAnt[i]->do_callback();
	}
}

/**
* This method redraws all the obstacles of the map on the windows
*/
void MotorAction::RedrawObstacles()
{
	for (int i = 0; i < numberDrawingObstacle; i++)
	{
		listDrawingObstacle[i]->do_callback();
	}
}

/**
* This method adds an Ant on the map.
* @a a pointer on the Ant to add on the map.
*/
void MotorAction::AddAnt(Ant * a)
{
	if (!a)
		return;

	map->AddAnt(a);
	AddDrawingAnt(new DrawingAnt(a));
}

/**
* This method adds an Ant on the map in a specified group.
* @a a pointer on the Ant to add on the map.
* @idGr the id of the group of ant to add @a.
*/
void MotorAction::AddAnt(Ant *a, unsigned int idGr)
{
	map->AddAnt(a, idGr);
	AddDrawingAnt(new DrawingAnt(a));
}

/**
* This method removes an Ant on the map.
*/
void MotorAction::RemoveAnt(unsigned int idAnt)
{
	map->RemoveAnt(idAnt);
	RemoveDrawingAnt(idAnt);
}

/**
* This method adds a GroupAnt on the map.
* @ga a pointer on the GroupAnt to add on the map.
*/
void MotorAction::AddGroup(GroupAnt * ga)
{
	map->AddGroup(ga);
	Ant** temp = ga->GetlistAnt();
	for (int i = 0; i < ga->GetnumberAnt(); i++)
		AddDrawingAnt(new DrawingAnt(temp[i]));
}

/**
* This method adds an obstacle on the map.
*/
void MotorAction::AddObstacle(Obstacle * obs)
{
	map->AddObstacle(obs);
	AddDrawingObstacle(new DrawingObstacle(obs));
}

/**
* Adds the Drawing to list @listDrawingAnt.
*/
void MotorAction::AddDrawingAnt(DrawingAnt* da)
{
	int n = map->GetnumberAnt();
	DrawingAnt ** temp = new DrawingAnt *[numberDrawingAnt + 1];
	for (int i = 0; i < n; i++)
		temp[i] = listDrawingAnt[i];
	temp[numberDrawingAnt] = da;

	if (listDrawingAnt)
		delete[] listDrawingAnt;
	listDrawingAnt = temp;
}

/**
* Removes the Drawing to list @listDrawingAnt.
*/
void MotorAction::RemoveDrawingAnt(unsigned int idAnt)
{
	if (!listDrawingAnt)
	{
		//throws exception
		return;
	}
	int index = 0;
	bool trouve = false;
	while(index <numberDrawingAnt && trouve)
		if (listDrawingAnt[index]->GetAnt()->GetId() == idAnt)
		{
			trouve = true;
			index++;
		}
	if (!trouve)
		return;
	DrawingAnt ** temp = new DrawingAnt*[numberDrawingAnt - 1];
	for (int i = 0; i < index; i++)
		temp[i] = listDrawingAnt[i];
	for (int i = index + 1; i < numberDrawingAnt; i++)
		temp[i - 1] = listDrawingAnt[i];

	if (listDrawingAnt[index])
		delete listDrawingAnt[index];
	delete[] listDrawingAnt;
	listDrawingAnt = temp;
}

/**
* Adds the Drawing to list @listDrawingObstacle.
*/
void MotorAction::AddDrawingObstacle(DrawingObstacle*obs)
{
	int n = map->GetNumberObstacle();
	DrawingObstacle ** temp = new DrawingObstacle *[numberDrawingObstacle + 1];
	for (int i = 0; i < n; i++)
		temp[i] = listDrawingObstacle[i];
	temp[numberDrawingObstacle] = obs;

	if (listDrawingObstacle)
		delete[] listDrawingObstacle;
	listDrawingObstacle = temp;
}

/**
* Adds the Drawing to list @listDrawingObstacle.
*/
void MotorAction::RemoveDrawingObstacle(int idObs)
{
	if (!listDrawingObstacle)
	{
		//throws exception
		return;
	}
	int index = 0;
	bool trouve = false;
	while (index < numberDrawingObstacle && trouve)
		if (listDrawingObstacle[index]->GetObstacle()->GetId() == idObs)
		{
			trouve = true;
			index++;
		}
	if (!trouve)
		return;
	DrawingObstacle ** temp = new DrawingObstacle*[numberDrawingObstacle - 1];
	for (int i = 0; i < index; i++)
		temp[i] = listDrawingObstacle[i];
	for (int i = index + 1; i < numberDrawingObstacle; i++)
		temp[i - 1] = listDrawingObstacle[i];

	if (listDrawingObstacle[index])
		delete listDrawingObstacle[index];
	delete[] listDrawingObstacle;
	listDrawingObstacle = temp;
}

//===================== Deplacement des fourmis ================================//

/**
* This method returns a new angle from an Ant.
*/
int MotorAction::GetNewAngle(Ant *)
{
	return 0;
}

/**
* This method returns a new speed from an Ant.
*/
int MotorAction::GetNewSpeed(Ant *)
{
	return 0;
}

/**
* This method change the position of an Ant on the map.
*/
void MotorAction::MoveAnt(Ant* a)
{
	//TEST
	Place p(a->GetLocation());
	int x = p.GetX() + (rand() % 11) - 5;
	int y = p.GetY() + (rand() % 11) - 5;
	a->SetLocation(Place(x, y));
}

/**
* This method moves all the ants to make a new image.
*/
void MotorAction::BuildNextImage()
{
	GroupAnt ** listGr = map->GetListGroup();
	int ng = map->GetNumberGroup();
	srand(time(NULL));

	for (int i = 0; i < ng; i++)
	{
		Ant ** listA = listGr[i]->GetlistAnt();
		int na = listGr[i]->GetnumberAnt();
		for (int j = 0; j < na; j++)
		{
			MoveAnt(listA[j]);
		}
	}
	RedrawAnts();
	windowsMap->flush();
}

/**
* this methods build severals images to make a video.
*/
void MotorAction::BuildVideo(int nbImages)
{
	std::cout << "\nBuildVideo\n";
	for (int i = 0; i < nbImages; i++)
	{
		BuildNextImage();
		std::cout << "S";
		Sleep(100);
	}
	std::cout << "\n";
}






