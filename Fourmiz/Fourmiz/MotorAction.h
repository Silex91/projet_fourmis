#pragma once
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include "DrawingAnt.h"
#include "DrawingObstacle.h"
#include "Map.h"

/**
* A class is used to build a video where ants move.
* This class inherits Fl_Double_Window in the FLTK library.
*/
class MotorAction// : public Fl_Double_Window
{
private:
	Map * map;

	/**
	* A Fl_Double_Window attribute.
	* This is the window printing the ants and obstacles.
	*/
	Fl_Double_Window * windowsMap;

	/**
	* An integer attribute.
	* The number of DrawingAnts in @listDrawingAnt
	*/
	int numberDrawingAnt;

	/**
	* A pointer on a table of DrawingAnt attribute.
	*/
	DrawingAnt ** listDrawingAnt;

	/**
	* An integer attribute.
	* The number of DrawingAnts in @listDrawingObstacle
	*/
	int numberDrawingObstacle;

	/**
	* A pointer on a table of DrawingObstacle attribute.
	*/
	DrawingObstacle ** listDrawingObstacle;

public:
	/**
	* A default constructor
	*/
	MotorAction();
	
	/**
	* A constructor 
	*/
	MotorAction(Map *);

	/**
	* The destrcutor of MotorAction class
	*/
	~MotorAction();

	//==================== Actualisation des dessins ==============================

	/**
	* This method redraws all the ants of the map on the windows
	*/
	void RedrawAnts();

	/**
	* This method redraws all the obstacles of the map on the windows
	*/
	void RedrawObstacles();

	//===================== Ajout de fourmis et d'obstacles ========================

	/**
	* This method adds an Ant on the map.
	*/
	void AddAnt(Ant*);

	/**
	* This method adds an Ant on the map in a specified group.
	*/
	void AddAnt(Ant*, unsigned int);

	/**
	* This method removes an Ant on the map.
	*/
	void RemoveAnt(unsigned int idAnt);

	/**
	* This method adds a GroupAnt on the map.
	*/
	void AddGroup(GroupAnt *);

	/**
	* This method adds an obstacle on the map.
	*/
	void AddObstacle(Obstacle*);

	/**
	* Adds the Drawing to list @listDrawingAnt.
	*/
	void AddDrawingAnt(DrawingAnt*);


	/**
	* Removes the Drawing to list @listDrawingAnt.
	*/
	void RemoveDrawingAnt(unsigned int idAnt);

	/**
	* Adds the Drawing to list @listDrawingObstacle.
	*/
	void AddDrawingObstacle(DrawingObstacle*);

	/**
	* Removes the Drawing to list @listDrawingObstacle.
	*/
	void RemoveDrawingObstacle(int index);

	//===================== Deplacement des fourmis ================================

	/**
	* This method returns a new angle from an Ant.
	*/
	int GetNewAngle(Ant *);

	/**
	* This method returns a new speed from an Ant.
	*/
	int GetNewSpeed(Ant *);

	/**
	* This method change the position of an Ant on the map.
	*/
	void MoveAnt(Ant*);

	/**
	* This method moves all the ants to make a new image.
	*/
	void BuildNextImage();

	/**
	* this methods build severals images to make a video.
	*/
	void BuildVideo(int );
};

