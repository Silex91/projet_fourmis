#include "Place.h"


/**
* A constructor by default.
* This constructor initialises values to 0.
*/
Place::Place() : x(0), y(0)
{}

/**
* A constructor by parameters.
* This constructor initialises x member with X value and y member with Y value.
* @X is an integer argument.
* @Y is an integer arggument.
*/
Place::Place(int X, int Y)
{
	x = X;
	y = Y;
}

/**
* A copy constructor.
* A constructor copying the Place Object argument.
* @p the adress of the object.
*/
Place::Place(const Place &p)
{
	x = p.x;
	y = p.y;
}

/**
* Getter x.
*/
int Place::GetX() const {return x;}

/**
* Setter x.
*/
void Place::SetX(int X) { x = X; }

/**
*Getter y.
*/
int Place::GetY() const { return y; }

/*
* Setter y.
*/
void Place::SetY(int Y) { y = Y; }

/**
* 
*/
void Place::operator=(Place const & paramPlace)
{
	x = paramPlace.GetX();
	y = paramPlace.GetY();
}

/**
* override of << operator.
* prints the information of the Place object.
*/
std::ostream& operator <<(std::ostream&os, Place&p)
{
	os << "(" << p.GetX() << " ; " << p.GetY() << ")";
	return os;
}