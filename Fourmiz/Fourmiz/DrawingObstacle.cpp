#include "DrawingObstacle.h"


/**
* The default value of @xPosition.
*/
int DrawingObstacle::xPosition = DEFAULT_XPOSITION;
/**
* The default value of @yPosition.
*/
int DrawingObstacle::yPosition = DEFAULT_YPOSITION;
/**
* The default value of @wSize.
*/
int DrawingObstacle::wSize = DEFAULT_WSIZE;
/**
* The default value of @hSize.
*/
int DrawingObstacle::hSize = DEFAULT_HSIZE;

int * cb_value = new int;

/**
* The callback function of the widget
@dObs a DrawingAnt pointer parameter.
@d a void parameter
*/
void cb_drawingObstacle(DrawingObstacle * dObs, void *d)
{
	dObs->damage(1);
}


/**
* A constructor by parameters.
* it creates an object to the default position and size of DrawingObstacle.
*/
DrawingObstacle::DrawingObstacle() : Fl_Widget(xPosition, yPosition, wSize, hSize)
{
	obstacle = new Obstacle();
	callback((Fl_Callback*)cb_drawingObstacle, cb_value);
}

/**
* A constructor by parameters.
* it creates an object to the position (X,Y) sized of WxH.
* @X, @Y, @W and @H are integer arguments.
*/
DrawingObstacle::DrawingObstacle(int X, int Y, int W, int H) : Fl_Widget(X, Y, W, H)
{
	obstacle = new Obstacle();
	callback((Fl_Callback*)cb_drawingObstacle, cb_value);
}

/**
* A constructor by parameters.
* it creates an object to the position (X,Y) sized of WxH.
* @X, @Y, @W and @H are integer arguments.
* @obst is an Obstacle argument.
*/
DrawingObstacle::DrawingObstacle(int X, int Y, int W, int H, Obstacle * obst) : Fl_Widget(X, Y, W, H)
{
	obstacle = obst;
	callback((Fl_Callback*)cb_drawingObstacle, cb_value);
}

/**
* A constructor by parameters.
* it creates an object to the default position and size of DrawingObstacle.
* @obst is an Obstacle argument.
*/
DrawingObstacle::DrawingObstacle(Obstacle *obst) : Fl_Widget(xPosition, yPosition, wSize, hSize)
{
	obstacle = obst;
	callback((Fl_Callback*)cb_drawingObstacle, cb_value);
}

/**
* A destructor.
* Does nothing.
*/
DrawingObstacle::~DrawingObstacle()
{
}

/**
* @overise
* A method with no arguments and returning nothing.
* This is the method called when we need to draw the widget.
*/
void DrawingObstacle::draw()
{
	fl_color(FL_BLACK);
	fl_begin_complex_polygon();

	for (int i = 0; i < obstacle->GetNumberPoint(); i++)
	{
		fl_vertex(obstacle->GetPoint(i)->GetX(), obstacle->GetPoint(i)->GetY());
	}

	fl_end_complex_polygon();
}

/**
* Getter obstacle.
*/
Obstacle * DrawingObstacle::GetObstacle()
{
	return obstacle;
}

/**
* Setter obstacle.
*/
void DrawingObstacle::SetObstacle(Obstacle * o)
{
	obstacle = o;
}
