#pragma once
#include "Place.h"

/**
 * A class used for area of the destination
 */
class Area
{
private:
	/**
	* A Place attribute.
	* Point1 is the left top point of the destination.
	*/
	Place Point1;

	/**
	* A Place attribute.
	* Point1 is the right bottom point of the destination.
	*/
	Place Point2;

public:
	/**
	* A constructor by default.
	*/
	Area();

	/**
	* A copy constructor.
	*/
	Area(const Area&);

	/**
	* A constructor by parameters.
	* @p1 is a Place. This is the left top point of the destination.
	* @p1 is a Place. This is the left top point of the destination.
	*/

	Area(Place p1, Place p2);
	/**
	* A destruuctor.
	* frees Point.
	*/
	~Area();

	/**
	* A method set a Place object to Point1.
	*/
	void SetPoint1(Place p);

	/**
	* A method set a Place object to Point2.
	*/
	void SetPoint2(Place p);

	/**
	* A method get Point1.
	*/
	Place GetPoint1();

	/**
	* A method get Point2.
	*/
	Place GetPoint2();

	/**
	* 
	*/
	Area &operator+=(const Area &);

	/**
	*/
	friend std::ostream& operator << (std::ostream&, Area&);

};
