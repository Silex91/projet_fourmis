#pragma once
#include<iostream>

/**
 * A class used for position of points
 */
class Place
{
private:
	int x;	/**<int variable x. This is the x-axis value of the point. */
	int y;	/**<int variable y. This is the y-axis value of the point. */

public:
	/**
	* A constructor by default.
	* This constructor initialises values to 0.
	*/
	Place();

	/**
	* A constructor by parameters.
	* This constructor initialises x member with X value and y member with Y value.
	* @X is an integer argument.
	* @Y is an integer arggument.
	*/
	Place(int X, int Y);

	/**
	* A copy constructor.
	* A constructor copying the Place Object argument.
	*/
	Place(const Place&);

	/**
	* Getter x.
	*/
	int GetX() const;

	/**
	* Setter x.
	* @X is an integer argument.
	*/
	void SetX(int X);

	/**
	* Getter y.
	*/
	int GetY() const;

	/**
	* Setter y.
	* @Y is an integer arggument.
	*/
	void SetY(int Y);

	/**
	* 
	*/
	void operator=(Place const& paramPlace);

	/**
	* 
	*/
	friend std::ostream& operator << (std::ostream&, Place&);

};

