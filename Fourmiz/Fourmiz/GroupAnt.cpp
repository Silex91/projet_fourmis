#include "GroupAnt.h"

/* Attributs :
	o static unsigned int nextIdGroup;
	o unsigned int idGroup;
	o unsigned int numberAnt;
	o Ant ** listAnt;
	o Place destination;
*/

unsigned int GroupAnt::nextIdGroup = 0;

/**
* A default constructor.
*/
GroupAnt::GroupAnt() {
	idGroup = nextIdGroup;
	nextIdGroup++;
	//--------------------
	numberAnt = 0; 
	listAnt = NULL;
}

/**
* A parameters constructor.
* @numberAntParam an unsigned int attribut : the number of ants in the group.
* @listAntParam a pointer on a table of ant : the list of ants in the group.
*/
GroupAnt::GroupAnt(unsigned int numberAntParam, Ant ** listAntParam) {
	idGroup = nextIdGroup;
	nextIdGroup++;
	//--------------------
	numberAnt = numberAntParam;
	listAnt = listAntParam;
}

/**
* A copy constructor.
* @GroupAntParam : a GroupAnt parameter : the group to copy.
*/
GroupAnt::GroupAnt(const GroupAnt & GroupAntParam) {
	idGroup = nextIdGroup;
	nextIdGroup++;
	//--------------------
	numberAnt = GroupAntParam.numberAnt;
	listAnt = new Ant*[numberAnt];
	for (int i = 0; i < numberAnt; i++)
	{
		listAnt[i] = new Ant(*GroupAntParam.listAnt[i]);
	}
}

/**
* The destructor of GroupAnt().
*/
GroupAnt::~GroupAnt()
{
	for (int i = 0; i < numberAnt; i++)
		if (listAnt[i])
			delete listAnt[i];
	if (listAnt)
		delete[] listAnt;
}

/**
* Getter nextIdGroup
*/
unsigned int GroupAnt::GetnextIdGroup() {
	return nextIdGroup;
}

/**
* Getter idGroup
*/
unsigned int GroupAnt::GetidGroup() {
	return idGroup;
}

/**
* Getter numberAnt
*/
unsigned int GroupAnt::GetnumberAnt() {
	return numberAnt;
}

/**
* Setter numberAnt
*/
void GroupAnt::SetnumberAnt(unsigned int numberAntParam) {
	numberAnt = numberAntParam;
}

/**
* Getter listAntParam
*/
Ant ** GroupAnt::GetlistAnt() {
	return listAnt;
} 

/**
* Setter listAntParam
*/
void GroupAnt::SetlistAnt(Ant ** listAntParam) {
	listAnt = listAntParam;
}


/**
 * addAnt in a method that add an Ant into GroupAnts.
 *
 * @param is the Ant to add into GroupAnts.
 * @return nothing.
*/
void GroupAnt::addAnt(Ant * antToAdd) {
	for (unsigned int i = 0; i < numberAnt; i++) {
		if (listAnt[i]->GetId() == antToAdd->GetId()) {
			//throws exception
			return;
		}
	}
	numberAnt++;
	Ant ** listAntTemp = new Ant *[numberAnt];
	for (unsigned int i = 0; i < numberAnt - 1; i++) {
		listAntTemp[i] = listAnt[i];
	}
	listAntTemp[numberAnt - 1] = antToAdd;
	if (listAnt != NULL) delete[] listAnt;
	listAnt = listAntTemp;
}

/**
* removeAnt in a method that remove an Ant into GroupAnts.
*
* @param is the Ant's id to remove into GroupAnts.
* @return nothing.
*/
void GroupAnt::removeAnt(unsigned int idAntToRemove) {
	unsigned int index = -1;
	for (unsigned int i = 0; i < numberAnt; i++) {
		if (listAnt[i]->GetId() == idAntToRemove) {
			index = i;
		}
		break;
	}
	if (index == -1)
	{
		//throws exception
		return;
	}
	Ant** listAntTemp = new Ant*[numberAnt - 1];
	for (int i = 0; i < index; i++)
		listAntTemp[i] = listAnt[i];
	for (int i = index + 1; i < numberAnt; i++)
		listAntTemp[i - 1] = listAnt[i];
	if (listAnt[index])
		delete listAnt[index];
	delete[] listAnt;
	listAnt = listAntTemp;
}

/**
 * clearListAnt in a method that clear all Ants into GroupAnts.
 *
 * @param is empty.
 * @return nothing.
*/
void GroupAnt::clearListAnt() {
	if (listAnt) {
		for (int i = 0; i < numberAnt; i++)
			if (listAnt[i])
				delete[] listAnt[i];
	}
	listAnt = NULL;
	numberAnt = 0;
}

