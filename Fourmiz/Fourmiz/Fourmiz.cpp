// Fourmiz.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <FL/Fl_BMP_Image.H>
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Hor_Value_Slider.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Image_Surface.H>
#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_JPEG_Image.H>

#include "Place.h"
#include "Area.h"
#include "Ant.h"
#include "Obstacle.h"
#include "DrawingAnt.h"
#include "DrawingObstacle.h"
#include "GroupAnt.h"
#include "Map.h"
#include "MotorAction.h"

#include <stdio.h>
#include <iostream>
#include <thread>

#define TEST
#define TEST_PLACE 
#define TEST_AREA 
#define TEST_ANT 
#define TEST_OBSTACLE 
#define TEST_GROUPANT 
#define TEST_DRAWINGANT1 
#define TEST_DRAWINGOBSTACLE1 
#define TEST_MAP1 
#define TEST_MOTORACTION

#pragma region TEST_place

void test_place()
{
	Place * p1 = new Place();
	Place * p2 = new Place(15, 80);
	Place p3;
	Place p4(14, 55);
	Place p5(*p2);

	std::cout << "p1 : " << *p1 << std::endl;
	std::cout << "p2 : " << *p2 << std::endl;
	std::cout << "p5 : " << p5 << std::endl;
	std::cout << "p4 : " << p4 << std::endl;

	p4.SetX(-15);
	p4.SetY(3500);

	std::cout << "\np4' : " << p4.GetX() << "; " << p4.GetY() << std::endl;

}

#pragma endregion

#pragma region TEST_AREA
void test_area()
{
	Area a1;
	Area a2(Place(10, 10), Place(50, 40));
	Area a3(a2);
	std::cout << "a1 = " << a1 << std::endl;
	std::cout << "a2 = " << a2 << std::endl;
	std::cout << "a3 = " << a3 << std::endl;
}
#pragma endregion

#pragma region TEST_ant

void test_ant()
{
	Ant * tabAnt = new Ant[10];
	Area dest(Place(0, 0), Place(0, 100));
	Ant a2(new Place(100, 100), dest, 3, true, 5, 5);
	Ant a3(a2);
	Ant a4;
	a4.SetXLocation(789); a4.SetWalkAngle(30); a4.SetDirection(0); a4.SetSpeed(10); a4.SetSafeDistance(4);

	std::cout << "\na0 : " << tabAnt->GetId() << "; " << tabAnt->GetXLocation() << " ; " << tabAnt->GetWalkAngle()
		<< " ; " << tabAnt->GetDirection() << " ; " << tabAnt->GetSpeed() << " ; " << tabAnt->GetSafeDistance();
	std::cout << "\na1 : " << tabAnt[9].GetId() << "; " << tabAnt[9].GetXLocation() << " ; " << tabAnt[9].GetWalkAngle()
		<< " ; " << tabAnt[9].GetDirection() << " ; " << tabAnt[9].GetSpeed() << " ; " << tabAnt[9].GetSafeDistance();
	std::cout << "\na2 : " << a2.GetId() << "; " << a2.GetXLocation() << " ; " << a2.GetWalkAngle()
		<< " ; " << a2.GetDirection() << " ; " << a2.GetSpeed() << " ; " << a2.GetSafeDistance();
	std::cout << "\na3 : " << a3.GetId() << "; " << a3.GetXLocation() << " ; " << a3.GetWalkAngle()
		<< " ; " << a3.GetDirection() << " ; " << a3.GetSpeed() << " ; " << a3.GetSafeDistance();
	std::cout << "\na4 : " << a4.GetId() << "; " << a4.GetXLocation() << " ; " << a4.GetWalkAngle()
		<< " ; " << a4.GetDirection() << " ; " << a4.GetSpeed() << " ; " << a4.GetSafeDistance();
}

#pragma endregion

#pragma region TEST_obstacle
void printObstacle(Obstacle o)
{
	std::cout << "\no0 : " << o.GetId() << " ; " << o.GetNumberPoint() << " ; " << o.GetCanBeClimbed() << " ; ";
	if (o.GetListPoint() != NULL)
	{
		std::cout << "non null : ";
		for (int i = 0; i < o.GetNumberPoint(); i++)
			std::cout << o.GetPoint(i)->GetX() << " - ";
	}
	else
		std::cout << "null";
}
void test_obstacle()
{
	Obstacle * o0 = new Obstacle[10];
	Place ** tab1 = new Place*[5];
	for (int i = 0; i < 5; i++)
		tab1[i] = new Place(i * 4, i * 12);
	Obstacle o1(5, tab1);

	printObstacle(*o0);
	printObstacle(o0[8]);
	printObstacle(o1);

	o1.AddPoint(new Place(123, 456));
	o1.RemovePoint(0);
	printObstacle(o1);
	o1.ClearListPoint();
	printObstacle(o1);

	o0[5].SetListPoint(tab1);
	o0[5].SetNumberPoint(5);
	printObstacle(o0[5]);

}

#pragma endregion

#pragma region TEST_drawingant
Fl_Double_Window * w;
Ant * a1;
int entier; //modifié par callback
void printDraw(DrawingAnt d)
{
	std::cout << "\nda : " << d.GetAnt()->GetId() << d.GetAnt()->GetXLocation();
}
void thread_red(DrawingAnt da1, DrawingAnt da2, DrawingAnt da3, DrawingAnt * da4)
{
	srand(time(NULL));
	for (int i = 0; i < 500; i = i + 20) {
		std::cout << "S";
		Sleep(100);
		da1.GetAnt()->SetXLocation(i);
		da1.GetAnt()->SetYLocation(rand() % 180 + 10);

		da2.GetAnt()->SetXLocation(i);
		da2.GetAnt()->SetYLocation(rand() % 180 + 10);

		da3.GetAnt()->SetXLocation(i);
		da3.GetAnt()->SetYLocation(rand() % 180 + 10);

		da4->GetAnt()->SetXLocation(i);
		da4->GetAnt()->SetYLocation(rand() % 180 + 10);
		da1.do_callback();
		da2.do_callback();
		da3.do_callback();
		da4->do_callback();

		w->flush();
	}
}
void thread_run(int m)
{
	int i = 0;
	while (i < m)
	{
		Fl::wait();
		std::cout << "#";
		i++;
	}
}
void test_drawingant()
{
	w = new Fl_Double_Window(500, 200, "TEST DRAWINGANT");
	a1 = new Ant(new Place(15, 51), Area(Place(0,0), Place(0,100)), 10, true, 5, 2);
	DrawingAnt da1;
	DrawingAnt da2(a1);
	DrawingAnt da3(50, 50, 20, 20);
	DrawingAnt * da4 = new DrawingAnt(new Ant(new Place(150, 100), Area(Place(0, 50), Place(0, 100)), 10, true, 3,3));
		
	printDraw(da1);
	printDraw(da2);
	printDraw(da3);
	printDraw(*da4);

	Fl::visual(FL_DOUBLE | FL_INDEX);
	w->end();
	w->free_position();
	w->show();

	std::thread th_red(thread_red, da1, da2, da3, da4);

	th_red.join();

	system("PAUSE");

}
#pragma endregion

#pragma region TEST_drawingObstacle
Fl_Double_Window * w2;

void thred_redObs(DrawingObstacle * obs1)
{
	srand(time(NULL));
	for (int i = 0; i < 400; i = i + 20) {
		std::cout << "S";
		Sleep(300);
		if(i%30 == 0)
			obs1->GetObstacle()->SetPoint(0, new Place(200, 200));
		else if (i % 10 == 0)
			obs1->GetObstacle()->SetPoint(0, new Place(20, 20));


		obs1->do_callback();

		w2->flush();
	}
}

void test_drawingobstacle()
{
	w2 = new Fl_Double_Window(800, 500, "TEST DRAWING_OBSTACLE");
	Fl::visual(FL_DOUBLE | FL_INDEX);

	Place ** tab1 = new Place *[5];
	Place ** tab2 = new Place *[5];
	for (int i = 0; i < 5; i++)
	{
		tab1[i] = new Place((i * 30 * (-i % 2 + 1)) + 50, (i * 10 * (-i % 2 + 1)) + 50);
		
	}
	Obstacle * o1 = new Obstacle(5, tab1);
	DrawingObstacle * do1 = new DrawingObstacle(o1);

	w2->end();
	w2->show();

	std::thread th2(thred_redObs, do1);
	th2.join();
	
	system("PAUSE");
	
}
#pragma endregion

#pragma region TEST_groupAnt
void test_groupAnt()
{
	Ant ** la = new Ant *[15];
	for (int i = 0; i < 15; i++)
		la[i] = new Ant(new Place(i * 10, i * 10), Area(Place(i * 10, 0), Place(i * 10 + 10, 0)), 10, false, 10, 10);
	GroupAnt * ga1 = new GroupAnt(15, la);

	ga1->addAnt(new Ant());
	ga1->removeAnt(12);
}
#pragma endregion

#pragma region TEST_map
void test_map()
{
	srand(time(NULL));
	Place ** lp = new Place*[5];
	lp[0] = new Place(150, 50);
	lp[1] = new Place(100, 100);
	lp[2] = new Place(150, 150);
	lp[3] = new Place(200, 100);
	lp[4] = new Place(160, 100);
	Obstacle ** lo = new Obstacle*[1];
	lo[0] = new Obstacle(5, lp);
	GroupAnt ** lg = new GroupAnt*[2];
	Area a1(Place(0, 50), Place(0, 250));
	Area a2(Place(50, 0), Place(300, 0));
	Ant** la1 = new Ant*[20];
	Ant** la2 = new Ant*[14];
	for (int i = 0; i < 20; i++)
		la1[i] = new Ant(new Place(rand() % 600, rand() % 300), a1, rand() % 360, true, rand() % 10, 5);

	for (int i = 0; i < 14; i++)
		la2[i] = new Ant(new Place(rand() % 600, rand() % 300), a2, rand() % 360, true, rand() % 10, 5);
	lg[0] = new GroupAnt(20, la1);
	lg[1] = new GroupAnt(14, la2);

	Map * m0 = new Map();
	Map * m1 = new Map(1, lo, 2, lg);
	Map * m2 = new Map(*m1);

	m1->AddAnt(new Ant(new Place(rand() % 600, rand() % 300), a1, rand() % 360, true, rand() % 10, 5));
	m1->AddAnt(new Ant(new Place(rand() % 600, rand() % 300), a1, rand() % 360, true, rand() % 10, 5), lg[1]->GetidGroup());
	m1->RemoveAnt(32);
	m1->ClearGroups();
}
#pragma endregion

#pragma region TEST_motorAction
void test_motorAction()
{
	srand(time(NULL));
	Place ** lp = new Place*[5];
	lp[0] = new Place(150, 50);
	lp[1] = new Place(100, 100);
	lp[2] = new Place(150, 150);
	lp[3] = new Place(200, 100);
	lp[4] = new Place(160, 100);
	Obstacle ** lo = new Obstacle*[1];
	lo[0] = new Obstacle(5, lp);
	GroupAnt ** lg = new GroupAnt*[2];
	Area a1(Place(0, 50), Place(0, 250));
	Area a2(Place(50, 0), Place(300, 0));
	Ant** la1 = new Ant*[20];
	Ant** la2 = new Ant*[14];
	for (int i = 0; i < 20; i++)
		la1[i] = new Ant(new Place(rand() % 600, rand() % 300), a1, rand() % 360, true, rand() % 10, 5);

	for (int i = 0; i < 14; i++)
		la2[i] = new Ant(new Place(rand() % 600, rand() % 300), a2, rand() % 360, true, rand() % 10, 5);
	lg[0] = new GroupAnt(20, la1);
	lg[1] = new GroupAnt(14, la2);

	Map * m1 = new Map(1, lo, 2, lg);
	MotorAction * ma = new MotorAction(m1);

	ma->BuildVideo(100);
	system("PAUSE");
}
#pragma endregion


void printTest()
{
	std::cout << "\n=====================================================\n\n";
}

#ifdef TEST
int main()
{
	printTest();
#ifdef TEST_PLACE
	printTest();
	test_place();
#endif // 

#ifdef TEST_AREA
	printTest();
	test_area();
#endif // 

#ifdef TEST_ANT
	printTest();
	test_ant();
#endif // 

#ifdef TEST_OBSTACLE
	printTest();
	test_obstacle();
#endif // 

#ifdef TEST_GROUPANT
	printTest();
	test_groupAnt();
#endif // 

#ifdef TEST_DRAWINGANT
	printTest();
	test_drawingant();
#endif // 

#ifdef TEST_DRAWINGOBSTACLE
	printTest();
	test_drawingobstacle();
#endif //

#ifdef TEST_MAP
	printTest();
	test_map();
#endif // 

#ifdef TEST_MOTORACTION
	printTest();
	test_motorAction();
#endif // 

	return 0;
}
#endif // TEST


#ifndef TEST

double args[6] = { 140, 140, 50, 0, 360, 0 };
const char* name[6] = { "X", "Y", "R", "start", "end", "rotate" };
class Dessin : public Fl_Widget
{
private:
	

public:
	Dessin(int X, int Y, int W, int H) : Fl_Widget(X, Y, W, H){}

	void draw()
	{
		fl_color(FL_BLUE);

		fl_begin_complex_polygon();
		//fl_begin_loop();
		fl_vertex(30, 30);
		fl_vertex(100, 30);
		fl_vertex(120, 120);
		fl_vertex(30, 100);
		//fl_end_loop();
		fl_gap();
		fl_vertex(40, 40);
		fl_vertex(40, 60);
		fl_vertex(60, 40);
		//fl_begin_loop();
		//fl_end_loop();
		fl_end_complex_polygon();
	}
};

class Drawing : public Fl_Widget {
private:
	int width;
	int heigh;
	Fl_JPEG_Image * ant;

public:
	void chdim(int x, int y) {
		width = x; heigh = y;
	}
	void draw() {
		
		fl_rectf(x()/3, y()/3, 20, 10, 0x55cc55);

		fl_color(FL_RED);
		fl_begin_points();
		fl_point(25, 25);
		fl_end_points();

		ant = new Fl_JPEG_Image("D:\\cassa\\Polytech\\S7\\Génie_Log\\Projet_fourmis\\imagesFourmis\\fourmiEast.jpg");
		if (ant != NULL)
		{
			fl_begin_line();
			fl_arc(width, width, 200, 100, 0, 359);
			fl_end_line();
			ant->draw(width, 150);
		}

		fl_begin_line();
		fl_pie(width, heigh, 100, 50, 0, 359);
		fl_end_line();
	}
public:
	Drawing(int X, int Y, int W, int H) : Fl_Widget(X, Y, W, H) {
		width = 0;
		heigh = 0;
		ant = NULL;
	}
};

Drawing *d;

int main(int argc, char** argv) {
	Fl_Double_Window window(1200, 500);
	/*Drawing drawing(10, 10, 280, 280);
	d = &drawing;

	drawing.chdim(50, 50);
	drawing.redraw();*/

	//Dessin *d1 = new Dessin(40, 110, 40, 40);
	//d1->position(100, 75);

	Fl_JPEG_Image * antEast = new Fl_JPEG_Image("D:\\cassa\\Polytech\\S7\\Génie_Log\\Projet_fourmis\\imagesFourmis\\fourmiEast.jpg");
	//antEast->draw(10, 10, antEast->w(), antEast->h());

	Ant * ant1 = new Ant(new Place(130, 70), 10, true, 2, 10);
	Ant * ant2 = new Ant(new Place(100, 65), 10, true, 2, 10);
	Ant * ant3 = new Ant(new Place(80, 255), 10, true, 2, 10);
	Ant * ant4 = new Ant(new Place(130, 300), 10, true, 2, 10);

	DrawingAnt * drawAnt1 = new DrawingAnt(ant1);
	DrawingAnt * drawAnt2 = new DrawingAnt(ant2);
	DrawingAnt * drawAnt3 = new DrawingAnt(ant3);
	DrawingAnt * drawAnt4 = new DrawingAnt(ant4);


	window.end();
	window.show(argc, argv);
	return Fl::run();
}


#endif // !Test