#include "DrawingAnt.h"


/**
* The default value of @xPosition.
*/
int DrawingAnt::xPosition = DEFAULT_XPOSITION;

/**
* The default value of @yPosition.
*/
int DrawingAnt::yPosition = DEFAULT_YPOSITION;

/**
* The default value of @wSize.
*/
int DrawingAnt::wSize = DEFAULT_WSIZE;

/**
* The default value of @hSize.
*/
int DrawingAnt::hSize = DEFAULT_HSIZE;


/**
* The default value of @ant_wSize.
*/
int DrawingAnt::ant_wSize = DEFAULT_ANT_WSIZE;

/**
* The default value of @ant_hSize.
*/
int DrawingAnt::ant_hSize = DEFAULT_ANT_HSIZE;


/**
* The callback function of the widget
@a a DrawingAnt pointer parameter.
@d a void parameter
*/
void cb_drawingAnt(DrawingAnt * da, void *d)
{
	da->damage(1);
}

int * value = new int;

/**
* A default constructor.
*/
DrawingAnt::DrawingAnt() : Fl_Widget(xPosition, yPosition, wSize, hSize)
{
	ant = new Ant();
	callback((Fl_Callback*)cb_drawingAnt, value);
}

/**
* A copy constructor.
*/
DrawingAnt::DrawingAnt(const DrawingAnt &d) : Fl_Widget(xPosition, yPosition, wSize, hSize)
{
	ant = d.ant;
	callback((Fl_Callback*)cb_drawingAnt, value);
}

/**
* A parameters constructor.
* @x, @y, @w and @h are integer arguments.
*/
DrawingAnt::DrawingAnt(int x, int y, int w, int h) : Fl_Widget(x, y, w, h)
{
	ant = new Ant();
	callback((Fl_Callback*)cb_drawingAnt, value);
}

/**
* A parameters constructor.
* @x, @y, @w and @h are integer arguments.
* @a is an Ant argument.
*/
DrawingAnt::DrawingAnt(int x, int y, int w, int h, Ant * a) : Fl_Widget(x, y, w, h)
{
	ant = a;
	callback((Fl_Callback*)cb_drawingAnt, value);
}

/**
* A parameters constructor.
* @a is an Ant argument.
*/
DrawingAnt::DrawingAnt(Ant * a) : Fl_Widget(xPosition, yPosition, wSize, hSize)
{
	ant = a;
	callback((Fl_Callback*)cb_drawingAnt, value);
}

/**
* A destructor.
* Does nothing.
*/
DrawingAnt::~DrawingAnt()
{
}

/**
* @overise
* A method with no arguments and returning nothing.
* This is the method called when we need to draw the widget.
*/
void DrawingAnt::draw()
{
	fl_color(FL_RED);
	//fl_rectf(ant->GetXLocation(), ant->GetYLocation(), ant_wSize, ant_hSize, 0x55cc55);

	fl_begin_line();
	fl_pie(ant->GetXLocation(), ant->GetYLocation(), ant_wSize, ant_hSize, 30, 300);
	fl_end_line();
}

/**
* Getter Ant.
*/
Ant * DrawingAnt::GetAnt()
{
	return ant;
}

/**
* Setter Ant.
@a a Ant pointer parameter.
*/
void DrawingAnt::SetAnt(Ant * a)
{
	ant = a;
}

