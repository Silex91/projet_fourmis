#include "Obstacle.h"
#include <iostream>

int Obstacle::nextIdObstacle = 1;


/**
* A constructor by default.
*/
Obstacle::Obstacle()
	:numberPoint(0), listPoint(NULL), canBeClimbed(false)
{
	idObstacle = nextIdObstacle;
	nextIdObstacle++;
}

/**
* A copy constructor by default.
*/
Obstacle::Obstacle(const Obstacle& o)
{
	idObstacle = nextIdObstacle;
	nextIdObstacle++;
	numberPoint = o.numberPoint;
	if (o.listPoint)
		listPoint = new Place*[numberPoint];
	else
		listPoint = NULL;
	for (int i = 0; i < numberPoint; i++)
		listPoint[i] = new Place(*o.listPoint[i]);
	canBeClimbed = o.canBeClimbed;
}

/**
* A constructor by parameter.
* @b is a boolean value.
*/
Obstacle::Obstacle(bool b)
	: numberPoint(0), listPoint(NULL)
{
	idObstacle = nextIdObstacle;
	nextIdObstacle++;
	canBeClimbed = b;
}

/**
* A constructor by parameters.
* @n is an Integer value. This is the index in the table @listPoint.
* @p is a pointer Place object. This is the list of points of the Obstacle.
*/
Obstacle::Obstacle(int n, Place **p)
{
	idObstacle = nextIdObstacle;
	nextIdObstacle++;
	numberPoint = n;
	listPoint = new Place*[n];
	for (int i = 0; i < numberPoint; i++)
		listPoint[i] = new Place(*p[i]);
}

/**
* A constructor by parameters.
* @n is an Integer value. This is the index in the table @listPoint.
* @p is a pointer Place object. This is the list of points of the Obstacle.
* @b il a boolean value.
*/
Obstacle::Obstacle(int n, Place **p, bool b)
{
	canBeClimbed = b;
	numberPoint = n;
	listPoint = new Place*[n];
	for (int i = 0; i < numberPoint; i++)
		listPoint[i] = new Place(*p[i]);
}

/**
* A destructor.
* frees @listPoint.
*/
Obstacle::~Obstacle()
{
	if (numberPoint > 0)
	{
		for (int i = 0; i < numberPoint; i++)
			delete listPoint[i];
		delete[] listPoint;
	}
}

/***********************************************************************************************/
/**
* A method adding a Place object in the listPoint.
*/
void Obstacle::AddPoint(Place * p)
{
	Place ** newList = new Place*[numberPoint + 1];
	for (int i = 0; i < numberPoint; i++)
	{
		newList[i] = listPoint[i];
	}
	newList[numberPoint] = p;
	numberPoint++;

	if (listPoint != NULL)
		delete[]listPoint;
	listPoint = newList;
}

/**
* A method removing a Place in the listPoint.
*/
void Obstacle::RemovePoint(int index)
{
	if (index < 0 || index >= numberPoint || numberPoint == 0)
	{
		//throw exception
		return;
	}
	
	Place ** newList = new Place*[numberPoint - 1];
	int j = 0;
	for (int i = 0; i < index; i++, j++)
	{
		newList[j] = listPoint[i];
	}
	for (int i = index + 1; i < numberPoint; i++, j++)
	{
		newList[j] = listPoint[i];
	}
	numberPoint--;
	if (listPoint[index])
		delete listPoint[index];
	if (listPoint != NULL)
		delete[]listPoint;
	listPoint = newList;
}

/**
* A method clearing the listPoint.
*/
void Obstacle::ClearListPoint()
{
	for (int i = 0; i < numberPoint; i++)
		if (listPoint[i])
			delete listPoint[i];
	if(listPoint)
		delete[]listPoint;
	listPoint = NULL;
	numberPoint = 0;
}


/***********************************************************************************************/

/**
* Getter canBeClimbed.
*/
bool Obstacle::GetCanBeClimbed() const
{
	return canBeClimbed;
}

/**
* Setter canBeClimed.
*/
void Obstacle::SetCanBeClimbed(bool b)
{
	canBeClimbed = b;
}

/**
* Getter numberPoint.
*/
int Obstacle::GetNumberPoint() const
{
	return numberPoint;
}

/**
* Setter numberPoint.
*/
void Obstacle::SetNumberPoint(int nb)
{
	numberPoint = nb;
}

/**
* Getter listPoint.
*/
Place ** Obstacle::GetListPoint() const
{
	return listPoint;
}


/**
* Setter listPoint.
*/
void Obstacle::SetListPoint(Place **p)
{
	if (listPoint != NULL)
		delete[] listPoint;
	listPoint = p;
}

/**
* Getter listPoint[i].
*/
Place * Obstacle::GetPoint(int i) const
{
	if (i < 0 || i >= numberPoint)
	{
		//throws Exception
		return new Place();
	}
	return listPoint[i];
}

/**
* Setter listPoint[i].
*/
void Obstacle::SetPoint(int i, Place * p)
{
	if (listPoint[i])
		delete listPoint[i];
	listPoint[i] = p;
}

/**
* Getter id.
*/
int Obstacle::GetId()
{
	return idObstacle;
}

/**
* Setter id.
*/
void Obstacle::SetId(int i)
{
	//si i est deja pris, lever une exception
	idObstacle = i;
}
