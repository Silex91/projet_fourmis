#pragma once
#include "Ant.h"
#include "GroupAnt.h"
#include "Obstacle.h"

/**
* A class containing the information of the terrain.
*/
class Map
{
private:
	/**
	* An integer attribute.
	* The number of Obstacles in @listObstacle.
	*/
	int numberObstacle;

	/**
	* A pointer on a table of pointer of Obstacle attribute.
	* It contains all the obstacles of the map.
	*/
	Obstacle ** listObstacle;

	/**
	* An integer attribute.
	* The number of groups ants of @listGroup.
	*/
	int numberGroup;

	/**
	* A pointer on a table of pointer of GroupAnt attribute.
	* It contains all the groups ants of the map.
	*/
	GroupAnt ** listGroup;

	/**
	* An integer attribute.
	* The total number of ants on the map.
	*/
	int numberAnt;

public:
	/**
	* A default constructor.
	*/
	Map();

	/**
	* A copy constructor.
	*/
	Map(const Map&);

	/**
	* A parameters constructor.
	*/
	Map(int, Obstacle**, int, GroupAnt**);

	/**
	* The destructor of Map class.
	*/
	~Map();

	/**
	* Adds an Ant in the first group of @listGroup.
	*/
	void AddAnt(Ant*);

	/**
	* Adds an Ant in the specified group of @listGroup.
	*/
	void AddAnt(Ant*, unsigned int);

	/**
	* Remove an Ant in the group of @listGroup which contains the ant.
	*/
	void RemoveAnt(unsigned int idAnt);

	/**
	* Removes an Ant in the specified group of @listGroup.
	*/
	void RemoveAnt(unsigned int idAnt, unsigned int idGroup);


	/**
	* Adds a group in the list @listGroup.
	*
	*/
	void AddGroup(GroupAnt*);

	/**
	* Removes a group in the list.
	*@id is the id of the group to remove in @listGroup.
	*/
	void RemoveGroup(int id);

	/**
	* Clears all the groups of @listGroup.
	*
	*/
	void ClearGroups();

	/**
	* Adds an obstacle in the list @listObstacle.
	*
	*/
	void AddObstacle(Obstacle*);

	/**
	* Removes an obstacle in the list @listObstacle.
	*@id the id of the obstacle to remove.
	*/
	void RemoveObstacle(int id);

	/**
	* Clears all the obstacles in @listObstacle.
	*
	*/
	void ClearObstacles();

	// ======================= Getter and Setter ==================================================

	/**
	* Getter of numberObstacle
	*/
	int GetNumberObstacle();

	/**
	* Setter of numberOstacle
	*/
	void SetNumberobstacle(int n);

	/**
	* Getter of listObstacle
	*/
	Obstacle** GetListObstacle();

	/**
	* Setter of listObstacle
	*/
	void SetListObstacle(Obstacle** lo);

	/**
	* Getter of numberGroup
	*/
	int GetNumberGroup();

	/**
	* Setter of numberGroup
	*/
	void SetNumberGroup(int n);

	/**
	* Getter of listGroup
	*/
	GroupAnt** GetListGroup();

	/**
	* Setter of listGroup
	*/
	void SetListGroup(GroupAnt** lg);

	/**
	* Getter of numberAnt
	*/
	int GetnumberAnt();

	/**
	* Setter of numberAnt
	*/
	void SetNumberAnt(int n);


};

