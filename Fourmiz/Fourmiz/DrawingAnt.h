#pragma once
#include <FL/fl_draw.H>
#include "Ant.h"

#define DEFAULT_XPOSITION 10;
#define DEFAULT_YPOSITION 10;
#define DEFAULT_WSIZE 100;
#define DEFAULT_HSIZE 100;

#define DEFAULT_ANT_WSIZE 20;
#define DEFAULT_ANT_HSIZE 20;

/**
* A class used for draw Ants.
* This class inherits Fl_Widget in the FLTK library.
* It allows to draw Ants using FLTK.
*/
class DrawingAnt :	public Fl_Widget
{
private:
	/**
	* A pointer on an Ant.
	* It contains the information of the ant to draw.
	*/
	Ant * ant;

	/**
	* An integer Value.
	* The default value of the X position of the DrawingAnt widget.
	*/
	static int xPosition;

	/**
	* An integer Value.
	* The default value of the Y position of the DrawingAnt widget.
	*/
	static int yPosition;

	/**
	* An integer Value.
	* The default value of the width of the DrawingAnt widget.
	*/
	static int wSize;

	/**
	* An integer Value.
	* The default value of the heigh of the DrawingAnt widget.
	*/
	static int hSize;

	/**
	* An integer Value.
	* The default value of the width of the DrawingAnt widget.
	*/
	static int ant_wSize;

	/**
	* An integer Value.
	* The default value of the heigh of the DrawingAnt widget.
	*/
	static int ant_hSize;

public:
	/**
	* A default constructor.
	*/
	DrawingAnt();

	/**
	* A copy constructor.
	*/
	DrawingAnt(const DrawingAnt&);

	/**
	* A parameter constructor.
	*/
	DrawingAnt(int, int, int, int);

	/**
	* A parameter constructor.
	*/
	DrawingAnt(int, int, int, int, Ant*);

	/**
	* A parameter constructor.
	*/
	DrawingAnt(Ant*);

	/**
	* A destructor.
	* Does nothing.
	*/
	~DrawingAnt();

	/**
	* @overise
	* A method with no arguments and returning nothing.
	* This is the method called when we need to draw the widget.
	*/
	void draw();

	/**
	* Getter Ant.
	*/
	Ant * GetAnt();

	/**
	* Setter Ant.
	*/
	void SetAnt(Ant*);

};

