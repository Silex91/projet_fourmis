#pragma once
#include<iostream>
#include "Ant.h"
#include "Place.h"

class GroupAnt
{
private:

	// ------------------- ATTRIBUTS --------------------

	/**
	* A static Integer attribute.
	* it is incremented each time an groupAnt is created.
	*/
	static unsigned int nextIdGroup;


	/**
	* An Integer attribute.
	* The id of the Group of ants.
	*/
	unsigned int idGroup;

	/**
	* An Integer attribute.
	* Represent the number of ants int the group.
	*/
	unsigned int numberAnt;

	/**
	* A pointer attribute on an Ant.
	* It contains the location where the ants have to go.
	*/
	Ant ** listAnt;


public:

	// -------------------- CONSTRUCTEURS ---------------------

	/**
	* A default constructor.
	*/
	GroupAnt();

	/**
	* A parameters constructor.
	*/
	GroupAnt(unsigned int, Ant **);
	
	/**
	* A copy consructor.
	*/
	GroupAnt(const GroupAnt&);

	/**
	* The destructor of GroupAnt.
	*/
	~GroupAnt();

	// ------------------- GETTERS/SETTERS --------------------

	/**
	* Getter @nextIdGroupt.
	*/
	static unsigned int GetnextIdGroup();

	/**
	* Getter @idGroup.
	*/
	unsigned int GetidGroup();

	/**
	* Getter @numberAnt.
	*/
	unsigned int GetnumberAnt();
	/**
	* Setter @numberAnt.
	*/
	void SetnumberAnt(unsigned int);
	/**
	* Getter @listAnt.
	*/
	Ant ** GetlistAnt();
	/**
	* Setter @listAnt.
	*/
	void SetlistAnt(Ant **);

	// ----------------------- METHODES -----------------------
	
	/**
	 * addAnt in a method that add an Ant into GroupAnts.
	 *
	 * @param is the Ant to add into GroupAnts.
	 * @return nothing.
	*/
	void addAnt(Ant * antToAdd);

	/**
	 * removeAnt in a method that remove an Ant into GroupAnts.
	 *
	 * @param is the Ant to remove into GroupAnts.
	 * @return nothing.
	*/
	void removeAnt(unsigned int idAntToRemove);

	/**
	 * clearListAnt in a method that clear all Ants into GroupAnts.
	 *
	 * @param is empty.
	 * @return nothing.
	*/
	void clearListAnt();

};

